package commoncodes

import (
	"bufio"
	"fmt"
	"os"
)

func Fileline(filePath string) []string {
	var linelist []string
	f, e := os.Open(filePath)
	if e != nil {
		fmt.Println("File error.")
	} else {
		buf := bufio.NewScanner(f)
		for {
			if !buf.Scan() {
				break
			}
			line := buf.Text()
			linelist = append(linelist, line)
		}
	}
	return linelist
}
