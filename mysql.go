package commoncodes

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type MysqlConf struct {
	Host     string
	Port     string
	Database string
	Username string
	Password string
	Charset  string
}

var DB *gorm.DB

func (m MysqlConf) InitDB() *gorm.DB {
	//driverName := viper.GetString("datasource.driverName")
	host := m.Host
	port := m.Port
	database := m.Database
	username := m.Username
	password := m.Password
	charset := m.Charset
	args := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=true",
		username,
		password,
		host,
		port,
		database,
		charset)
	db, err := gorm.Open(mysql.Open(args), &gorm.Config{})
	if err != nil {
		//panic("fail to connect database, err:" + err.Error())
		fmt.Println("fail to connect database, err:" + err.Error())
	}

	DB = db
	return db
}

func GetDB() *gorm.DB {
	return DB
}
